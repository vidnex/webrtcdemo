var layout
var role;
var session;
var centerName = null;
var roomName = null;
var publisher = null;
var publisherName;
var publishStatus = false;
var chatMessages = [];
for (i=1; i< 100; i++)
  chatMessages[i] = "";

if(!String.linkify) {
  String.prototype.linkify = function() {

    // http://, https://, ftp://
    var urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;

    // www. sans http:// or https://
    var pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

    // Email addresses
    var emailAddressPattern = /\w+@[a-zA-Z_]+?(?:\.[a-zA-Z]{2,6})+/gim;

    return this
      .replace(urlPattern, '<a href="$&" target="_blank">$&</a>')
      .replace(pseudoUrlPattern, '$1<a href="http://$2" target="_blank">$2</a>')
      .replace(emailAddressPattern, '<a href="mailto:$&">$&</a>');
  };
}

function subscribeStudent(stream)
{
  streamConectionId = stream.connection.connectionId;
  divId = "stream-" + streamConectionId; 
  $("#layoutContainer").append('<div id="'+ divId +'"></div>');
  session.subscribe(stream, divId, 
    {
      width: "100%", 
      height: "100%",
      style: {
        nameDisplayMode: "on"
      }
    }
  );
}

function sendChat(message)
{
  session.signal({
    type: "chat",
    data: {
      name: publisherName,
      text: message
    }
  },
  function(error) {});    
}

function showChatText(name, text)
{
  for (i=1; i< 100; i++)
    chatMessages[i-1] = chatMessages[i];
  chatMessages[99] = "<strong>" + name + "</strong>" + ": " + text.linkify() + "</br>";
  result = "";
  for (i=0; i< 100; i++)
    result += chatMessages[i];
  $("#displayArea").html(result);
  var wtf = $('#displayArea');
  var height = wtf[0].scrollHeight;
  wtf.scrollTop(height);
}

$(function() {
  $("#chatMessage").keyup(function (e) {
    if (e.keyCode == 13) {
      sendChat(this.value);
      $("#chatMessage").val("");
    }
  });
  $("#menuVideoOn").click(function (e) {
    $("#menuModal").hide();
    if ( publishStatus == false )
    {
      publisher = session.publish("layoutContainer", 
      {
        insertMode: "append",
        name: publisherName,
        style: {
          nameDisplayMode: "on"
        }          
      });
      publishStatus = true
      layout();
    }    
  });
  $("#menuVideoOff").click(function (e) {
    $("#menuModal").hide();
    if ( publishStatus == true )
    {
      session.unpublish(publisher);
      publishStatus = false
    }
    layout();
  });
  $("#openMenu").click(function (e) {
    $("#menuModal").show();
  });
  $("#closeMenu").click(function (e) {
    $("#menuModal").hide();
  });   
  $("#chatMute").click(function (e) {
    session.signal({
      type: "mute",
      data: {
        name: publisherName,
        text: "MICROS OFF"
      }
    },
    function(error) {});
  });    
  $("#sendAdvisor").click(function (e) {    
    $("#sendAdvisor").hide();
    $("#waitAdvisor").show();

    showChatText(publisherName, "HELP!");
    $.get( "/"+roomName+"/"+centerName )
      .done(function( data ) {
    });     
  });  
});

function init_session(apiKey, session_id, token, cname, rname, uname)
{
  centerName = cname;
  roomName = rname;
  publisherName = uname;

  layout = TB.initLayoutContainer( document.getElementById("layoutContainer"),
  {
    fixedRatio: true,
    animate: true,
    bigClass: "OT_big",
    bigPercentage: 0.85,
    bigFixedRatio: false,
    easing: "swing"
  }).layout; 

  session = TB.initSession(apiKey, session_id);  

  session.connect(token, function(error) {
    var objectData = JSON.parse(session.connection.data);
    role = objectData.role

    if ( role == 't' ||
         role == 's' )
    {
      //showChatText(objectData.uname, "IN");
      sendChat("IN");
    }
    
    if ( role == 't' || role == 'a')
    {      
      publisher = session.publish("publisherContainer", 
        {
          insertMode: "append",
          name: uname,
          style: {
            nameDisplayMode: "on"
          }
        });
    }
    /*else if ( role == 's')
    {
      publisher = session.publish("layoutContainer", 
        {
          insertMode: "append",
          name: uname,
          style: {
            nameDisplayMode: "on"
          }          
        });
    }*/
    layout();
  });

  session.on("streamCreated", function(event) {

    objectData = JSON.parse(event.stream.connection.data);

    if ( objectData.role == 't' ||
         objectData.role == 's' ||
           ( objectData.role == 'a' && role != 's')
       )
    {
      showChatText(objectData.uname, "START VIDEO");
    }

    // I'm a viewer
    if ( role == 'v' )
    {
      subscribeStudent(event.stream);     
      layout();
    }
    // I'm an advisor
    else if ( role == "a" )
    {
      if ( objectData.role == 't' )
      {        
        session.subscribe(event.stream, 
          "advisorContainer", 
          {
            insertMode: "append",
            style: {
              nameDisplayMode: "on"
            }
          }
        );
      }
      else
        subscribeStudent(event.stream);     
      layout();      
    }
    // I'm a teacher
    else if ( role == "t" )
    {
      if ( objectData.role == 'a' )
      {
        $("#sendAdvisor").hide();
        $("#waitAdvisor").hide();
        session.subscribe(event.stream, 
          "advisorContainer", 
          {
            insertMode: "append",
            style: {
              nameDisplayMode: "on"
            }
          }
        );
      }
      else
        subscribeStudent(event.stream);     
      layout();      
    }
    // I'm a student
    else
    {
      // a student only view the teacher
      if ( objectData.role == 't' )
      {
        session.subscribe(event.stream, 
          "layoutContainer", 
          {
            insertMode: "append",
            style: {
              nameDisplayMode: "on"
            }
          }
        );
        layout();
      }
    }
  });

  /*session.on("connectionDestroyed", function(event) {
    alert("DESTRUIDO");
  });

  session.on("connectionCreated", function(event) {
    alert("CREADO");
  });*/

  session.on("streamDestroyed", function(event) {
    objectData = JSON.parse(event.stream.connection.data);

    if ( objectData.role == 't' ||
         objectData.role == 's' ||
        ( objectData.role == 'a' && role != 's')
    )
    {
      showChatText(objectData.uname, "STOP VIDEO");
    }    
    
    if ( role == 't' && objectData.role == 'a' )
    {
      $("#sendAdvisor").show();
    }
    else
    {
      $("#stream-" + event.stream.connection.connectionId).remove();
      layout();
    }    
  });

  session.on("signal:chat", function(event) {
    showChatText(event.data["name"], event.data["text"]);
  });

  session.on("signal:mute", function(event) {
    showChatText(event.data["name"], event.data["text"]);
    if ( role == 's' && publisher != null )
    {
      publisher.publishAudio(false);
    }
  });

  var resizeTimeout;
  window.onresize = function() {
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(function () {
      layout();
    }, 20);
  };
}

