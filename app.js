// ***
// *** Required modules
// ***
//require('newrelic');
var express = require('express');
var mailer = require('express-mailer');
var OpenTokLibrary = require('opentok');
//var i18n = require('il8n-abide');

// ***
// *** OpenTok Constants for creating Session and Token values
// ***
//var OTKEY = '38953222';  //process.env.TB_KEY;
//var OTSECRET = '50b4adf245fd0457fd49b51b6a3b381ff97e4364'; //process.env.TB_SECRET;
var OTKEY = '45729862';  //process.env.TB_KEY;
var OTSECRET = '5f86bfefe601f89a946151fcb3a550ed42f6b288'; //process.env.TB_SECRET;
var OpenTokObject = new OpenTokLibrary.OpenTokSDK(OTKEY, OTSECRET);

// ***
// *** Setup Express to handle static files in public folder
// *** Express is also great for handling url routing
// ***
var app = express();
app.use(express.static(__dirname + '/public'));
app.set( 'views', __dirname + "/views");
app.set( 'view engine', 'ejs' );

//*** https handle
env = process.env.NODE_ENV || 'development';

var forceSsl = function (req, res, next) {
  if (req.headers['x-forwarded-proto'] !== 'https') {
    return res.redirect(['https://', req.get('Host'), req.url].join(''));
  }
  return next();
};

app.use(forceSsl);
//****

// ***
// *** Setup Express-mailer to handle mailer
// ***
mailer.extend(app, {
  from: 'no-reply@vidnex.com',
  host: 'smtp.gmail.com', // hostname
  secureConnection: true, // use SSL
  port: 465, // port for secure SMTP
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
  auth: {
    user: 'info@vidnex.com',
    pass: 'testtest1'
  }
});

var rooms = {};

// ***  a user comes in with a room id - and no view or class position 

app.get("/:rid/:role/:rname/:uname", function(req, res) {
  console.log(req.url);
  add_to_room(res, req.params.rid.split(".json"), 
                   req.params.role.split(".json"),
                   req.params.rname.split(".json"),
                   req.params.uname.split(".json"))
});

app.get('/:rid/:rname', function (req, res, next) {
  var rid = req.params.rid.split(".json")[0];
  var rname = req.params.rname.split(".json")[0];
  app.mailer.send('mail_assistant', {
    to: 'info@vidnex.com', // REQUIRED. This can be a comma delimited string just like a normal email to field. 
    subject: 'Solicitud de asistenacia Vidnex', // REQUIRED.
    host: req.get('host'),
    rid: rid,
    rname: rname
  }, function (err) {
    if (err) {
      // handle error
      console.log(err);
      res.send('KO');
      return;
    }
    res.send('OK');
  });
});

function add_to_room(res, split_path, split_role, split_rname, split_uname){

  var rid = split_path[0];
  var role = split_role[0];
  var rname = split_rname[0];
  var uname = split_uname[0];

   // *** Generate sessionId if there are no existing session Id's
  if( !rooms[rid] ){
    OpenTokObject.createSession(function(session_id){
      rooms[rid] = session_id;
      returnRoomResponse( res, session_id, role, rid, rname, uname);
    });
  }else{
    returnRoomResponse( res, rooms[rid], role, rid, rname, uname); 
  }
}

function returnRoomResponse( res, session_id, role, rid, rname, uname ){

  console.log(role + '-' + rname + '-' + uname);

  switch (role)
  {
    case 't':
      return returnTeacherRoomResponse(res, rid, rname, uname, session_id);
    break; 
    case 's':
      return returnStudentRoomResponse(res, rid, rname, uname, session_id);
    break;
    case 'v':
      return returnViewerRoomResponse(res, rid, rname, uname, session_id);
    break;
    case 'a':
      return returnAdvisorRoomResponse(res, rid, rname, uname, session_id);
    break;    
  }
}

function returnTeacherRoomResponse( res, rid, rname, uname, session_id){
  var data = {};

  data.apiKey = OTKEY;
  data.cname = rname;
  data.rname = rid;
  data.uname = uname;
  data.session_id = session_id;
  data.token = OpenTokObject.generateToken({
    session_id: session_id, 
    role: OpenTokLibrary.RoleConstants.MODERATOR, 
    connection_data: '{"role":"t","uname":"' + uname + '"}'
  });
  res.render( "new_teacher_room", data );
}

function returnStudentRoomResponse( res, rid, rname, uname, session_id){
  var data = {};

  data.apiKey = OTKEY;
  data.cname = rname;
  data.rname = rid;
  data.uname = uname;
  data.session_id = session_id;
  data.token = OpenTokObject.generateToken({
    session_id: session_id, 
    role: OpenTokLibrary.RoleConstants.PUBLISHER, 
    connection_data: '{"role":"s","uname":"' + uname + '"}'
  });
  res.render( "new_student_room", data );
}

function returnViewerRoomResponse( res, rid, rname, uname, session_id){
  var data = {};

  data.apiKey = OTKEY;
  data.cname = rname;
  data.rname = rid;
  data.uname = uname;
  data.session_id = session_id;
  data.token = OpenTokObject.generateToken({
    session_id: session_id, 
    role: OpenTokLibrary.RoleConstants.MODERATOR, 
    connection_data: '{"role":"v","uname":"' + uname + '"}'
  });
  res.render( "new_viewer_room", data );
}

function returnAdvisorRoomResponse( res, rid, rname, uname, session_id){
  var data = {};

  data.apiKey = OTKEY;
  data.cname = rname;
  data.rname = rid;
  data.uname = uname;
  data.session_id = session_id;
  data.token = OpenTokObject.generateToken({
    session_id: session_id, 
    role: OpenTokLibrary.RoleConstants.MODERATOR, 
    connection_data: '{"role":"a","uname":"' + uname + '"}'
  });
  res.render( "new_advisor_room", data );
}

// ***
// *** start server, listen to port 
// ***
var port = process.env.PORT || 5000;
app.listen(port);
